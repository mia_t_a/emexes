'use strict'

// Load plugins
require('dotenv').config()
const gulp = require('gulp')
const plumber = require('gulp-plumber')
const autoprefixer = require('gulp-autoprefixer')
const browserSync = require('browser-sync').create()
const del = require('del')
const rename = require('gulp-rename')
const gcmq = require('gulp-group-css-media-queries')
const less = require('gulp-less')
const cleanCSS = require('gulp-clean-css')
const fileinclude = require('gulp-file-include')
const svgSprite = require('gulp-svg-sprites')
const concat = require('gulp-concat')
const sourcemaps = require('gulp-sourcemaps')
const argv = require('yargs').argv
const gulpif = require('gulp-if')
const webp = require('gulp-webp')
const imagemin = require('gulp-imagemin')
const babel = require('gulp-babel')

const fs = require('fs')

// BrowserSync
function server(done) {
  browserSync.init({
    server: {
      baseDir: './build/',
    },
    open: false,
    notify: false,
    ghostMode: false
  })
  done()
}

// Clean assets
function clean() {
  return del(['./build'])
}

// Images
function images() {
  return gulp
      .src(['./src/img/**/*.*'])
      .pipe(plumber())
      .pipe(
          imagemin({
            progressive: true,
          })
      )
      .pipe(gulp.dest('./build/img/'))
      .pipe(browserSync.stream())
}

// favicon
function favicon() {
  return gulp
      .src(['./src/favicon/**/*.*'])
      .pipe(plumber())
      .pipe(gulp.dest('./build/favicon/'))
      .pipe(browserSync.stream())
}

// Webp
function convertWebp() {
  return gulp
      .src(['./src/img/**/*.{jpg,jpeg,png}'])
      .pipe(plumber())
      .pipe(
          gulpif(
              argv.prod,
              webp({
                quality: 99,
                method: 6,
              })
          )
      )
      .pipe(
          gulpif(
              !argv.prod,
              webp({
                method: 0,
              })
          )
      )
      .pipe(gulp.dest('./build/img/'))
      .pipe(browserSync.stream())
}

// svg sprite
function createSvgSprite() {
  return gulp
      .src(['./src/img/svg/icons/**/*.svg'])
      .pipe(plumber())
      .pipe(
          svgSprite({
            svgId: 'ico-%f',
            mode: 'symbols',
            preview: false,
          })
      )
      .pipe(
          rename(function (path) {
            path.basename = 'sprite'
            path.extname = '.svg'
          })
      )
      .pipe(gulp.dest('./build/img/'))
      .pipe(browserSync.stream())
}

// CSS task
function css() {
  return gulp
      .src('./src/less/*.less')
      .pipe(plumber())
      .pipe(gcmq())
      .pipe(gulpif(!argv.prod, sourcemaps.init()))
      .pipe(
          less({
            modifyVars: {
              '@APP_WINDOW_SIZE_XXL': process.env.APP_WINDOW_SIZE_XXL + 'px',
              '@APP_WINDOW_SIZE_XL': process.env.APP_WINDOW_SIZE_XL + 'px',
              '@APP_WINDOW_SIZE_LG': process.env.APP_WINDOW_SIZE_LG + 'px',
              '@APP_WINDOW_SIZE_MD': process.env.APP_WINDOW_SIZE_MD + 'px',
              '@APP_WINDOW_SIZE_TABLETS': process.env.APP_WINDOW_SIZE_TABLETS + 'px',
              '@APP_WINDOW_SIZE_SM': process.env.APP_WINDOW_SIZE_SM + 'px',
              '@APP_WINDOW_SIZE_XS': process.env.APP_WINDOW_SIZE_XS + 'px',
              '@APP_WINDOW_SIZE_XXS': process.env.APP_WINDOW_SIZE_XXS + 'px',
            },
          })
      )
      .pipe(
          autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false,
          })
      )
      .pipe(gulpif(argv.prod, cleanCSS()))
      .pipe(
          rename({
            extname: '.css',
          })
      )
      .pipe(gulpif(!argv.prod, sourcemaps.write('./')))
      .pipe(gulp.dest('./build/css/'))
      .pipe(browserSync.stream())
}

// CSS copy
function cssCopy() {
  return gulp
      .src('./src/css-copy/**/*.css')
      .pipe(plumber())
      .pipe(gulpif(!argv.prod, sourcemaps.init()))
      .pipe(gulpif(argv.prod, cleanCSS()))
      .pipe(gulpif(!argv.prod, sourcemaps.write('./')))
      .pipe(gulp.dest('./build/css'))
      .pipe(browserSync.stream())
}

//CSS libs
function cssLibs() {
  return gulp
      .src('./src/libs/css/**/*.css')
      .pipe(plumber())
      .pipe(gulpif(!argv.prod, sourcemaps.init()))
      .pipe(concat('libs.css'))
      .pipe(cleanCSS())
      .pipe(gulpif(argv.prod, cleanCSS()))
      .pipe(gulpif(!argv.prod, sourcemaps.write('./')))
      .pipe(gulp.dest('./build/css'))
      .pipe(browserSync.stream())
}

//HTML task
function html() {
  return gulp
      .src(['./src/*.html'])
      .pipe(plumber())
      .pipe(fileinclude())
      .pipe(gulp.dest('./build/'))
      .pipe(browserSync.stream())
}

// Video
function video() {
  return gulp
      .src('./src/video/**/*.*')
      .pipe(gulp.dest('./build/video'))

      .pipe(browserSync.stream())
}

// Fonts
function fonts() {
  return gulp.src('./src/fonts/**/*.*').pipe(gulp.dest('./build/fonts')).pipe(browserSync.stream())
}

// scriptsLibs
function scriptsLibs() {
  return (
      gulp
          .src(['./src/libs/js/**/*.js'])
          .pipe(plumber())
          .pipe(concat('libs.js'))
          // .pipe(gulpif(argv.prod, babel({
          //   presets: ['@babel/env']
          // })))
          .pipe(gulp.dest('./build/js'))
          .pipe(browserSync.stream())
  )
}

// scriptsCopy
function scriptsCopy() {
  return gulp
      .src(['./src/js-copy/**/*.*'])
      .pipe(plumber())
      .pipe(
          gulpif(
              argv.prod,
              babel({
                presets: ['@babel/env'],
                minified: true,
              })
          )
      )
      .pipe(gulp.dest('./build/js'))
      .pipe(browserSync.stream())
}

// Transpile, concatenate and minify scripts
function scripts() {
  return gulp
      .src(['./src/js/**/*.js'])
      .pipe(plumber())
      .pipe(concat('main.js'))
      .pipe(
          gulpif(
              argv.prod,
              babel({
                presets: ['@babel/env'],
                minified: true,
              })
          )
      )
      .pipe(gulp.dest('./build/js/'))
      .pipe(browserSync.stream())
}

// Watch files
function watchFiles() {
  gulp.watch('./src/less/**/*', css)
  gulp.watch('./src/*.html', html)
  gulp.watch('./src/modules/**/*.html', html)
  gulp.watch('./src/js/**/*.js', scripts)
  gulp.watch('./src/img/**/*.*', images)
  gulp.watch('./src/img/svg/main/**/*.svg', createSvgSprite)
  gulp.watch('./src/css-copy/**/*.css', cssCopy)
  gulp.watch('./src/libs/css/**/*.css', cssLibs)
  gulp.watch('./src/video/**/*.*', video)
  gulp.watch('./src/libs/js/**/*.js', scriptsLibs)
  gulp.watch('./src/js-copy/**/*.*', scriptsCopy)
  gulp.watch('./src/fonts/**/*.*', fonts)
  gulp.watch('./src/favicon/**/*.*', favicon)
  gulp.watch('./src/img/**/*.{jpg,jpeg,png}', convertWebp)
}

// define complex tasks
const js = gulp.series(scripts)
const build = gulp.series(
    clean,
    gulp.parallel(
        css,
        html,
        images,
        favicon,
        convertWebp,
        createSvgSprite,
        cssCopy,
        cssLibs,
        video,
        scriptsLibs,
        scriptsCopy,
        fonts,
        js
    )
)
const watch = gulp.series(build, gulp.parallel(watchFiles, server))

// export tasks
exports.images = images
exports.convertWebp = convertWebp
exports.css = css
exports.js = js
exports.html = html
exports.createSvgSprite = createSvgSprite
exports.cssCopy = cssCopy
exports.cssLibs = cssLibs
exports.video = video
exports.scriptsLibs = scriptsLibs
exports.scriptsCopy = scriptsCopy
exports.fonts = fonts
exports.clean = clean
exports.build = build
exports.watch = watch
exports.default = build
